package com.mobile.theirgithub.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.mobile.theirgithub.adapters.UserInformationAdapter
import com.mobile.theirgithub.BR
import com.mobile.theirgithub.R
import com.mobile.theirgithub.interfaces.ViewModeResponse
import com.mobile.theirgithub.viewModel.UserViewModel
import com.mobile.theirgithub.databinding.ActivitySingleUserDetailBinding
import kotlinx.android.synthetic.main.activity_single_user_detail.*


class SingleUserDetailActivity : AppCompatActivity(),
    ViewModeResponse {


    var binding:ActivitySingleUserDetailBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivitySingleUserDetailBinding>(this,
            R.layout.activity_single_user_detail
        )
        UserViewModel(
            this,
            intent.getStringExtra("Name"),
            this
        )

    }

    //Apis response
    override fun success(viewModel: UserViewModel) {
        runOnUiThread {

            binding!!.setVariable(BR.userViewModel, viewModel)
            binding!!.setLifecycleOwner(this)

            recycleView.adapter =
                UserInformationAdapter(this, viewModel)

        }
    }

    //failure of response comes here
    override fun failure(msg: String) {
        runOnUiThread(Runnable {
            Toast.makeText(this,msg,Toast.LENGTH_LONG).show()
        })
    }



}
