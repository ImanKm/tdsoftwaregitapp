package com.mobile.theirgithub.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.mobile.theirgithub.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener(View.OnClickListener {
            validate()
        })
    }




    //validate whether user entered username or not
    fun validate(){
        if( !editText1.text.toString().equals("")){
            sendByIntent()
        }else{
            Toast.makeText(this,"Please enter username",Toast.LENGTH_LONG).show()
        }
    }

    //go to detail information of entered username
    fun sendByIntent(){
        var intent = Intent(this, SingleUserDetailActivity::class.java)
        intent.putExtra("Name",editText1.text.toString())
        startActivity(intent)
    }

}
