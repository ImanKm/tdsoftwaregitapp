package com.mobile.theirgithub.utils

import java.text.SimpleDateFormat
import java.util.*



class DateUtil {

   companion object {
       private val compliteFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

       fun getYear(dt:String):String{
           var date = Date()
           try {
                date = compliteFormat.parse(dt)
           } catch (e: Exception) {
               e.printStackTrace()
           }
           val cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"))
           cal.time = date
           return ""+cal.get(Calendar.YEAR);
       }
   }

}