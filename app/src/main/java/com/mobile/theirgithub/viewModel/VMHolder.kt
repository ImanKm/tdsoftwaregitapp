package com.mobile.theirgithub.viewModel

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import retrofit2.*
import java.io.IOException
import com.mobile.theirgithub.configuration.Configs
import com.mobile.theirgithub.interfaces.OnClickItemListener
import com.mobile.theirgithub.interfaces.ViewModeResponse
import com.mobile.theirgithub.network.ApiClient
import com.mobile.theirgithub.network.ApiInterfaces
import com.mobile.theirgithub.network.ErrorHandlingAdapter


class UserViewModel( activity: Activity, val userName: String, val viewModeResponse: ViewModeResponse): ViewModel(),
    OnClickItemListener {

    var userModel: MutableLiveData<UserModel> = MutableLiveData()
    var repositoryModel:  MutableLiveData<ArrayList<RepositoryModel>?> = MutableLiveData()
    var userFlag = false
    var repositoriesFlag = false
    internal var apiService : ApiInterfaces
    var activity:Activity

    init {
          this.activity = activity
          apiService = ApiClient.getClient(activity).create(ApiInterfaces::class.java)
          getUser()
          getRepositories()
    }

    override fun onItemClick(url:String?) {
        intentToBrowser(url)
    }

    fun intentToBrowser(url:String?){
       if (url != null && !url.equals("")) {
           val intent = Intent(Intent.ACTION_VIEW)
           intent.data = Uri.parse(url)
           activity.startActivity(intent)
       }
    }

    companion object {
        @JvmStatic
        @BindingAdapter("profileImage")
        fun loadImage(view: ImageView, imageUrl: String) {
            Glide.with(view.context)
                .load(imageUrl)
                .into(view)
        }
    }

    private fun getUser() {
        val call = apiService.user(userName,
            Configs.CLIENTID,
            Configs.CLIENTSECRET)
        call.enqueue(object : ErrorHandlingAdapter.MyCallback<UserModel> {

            override fun clientError(response: Response<*>) {
                viewModeResponse.failure("CLIENT ERROR!")
            }

            override fun success(response: Response<UserModel>) {
                System.out.println("SUCCESS! " + response.body())
                if (response.body() != null) {
                    userFlag = true
                    userModel.postValue( response.body()!!)
                    responsePatcher()
                } else {
                    viewModeResponse.failure(response.code().toString())
                }
            }

            override fun unauthenticated(response: Response<*>) {
                println("UNAUTHENTICATED")
                viewModeResponse.failure("UNAUTHENTICATED ERROR!")
            }

            override fun serverError(response: Response<*>) {
                System.out.println("SERVER ERROR " + response.code() + " " + response.message())
                viewModeResponse.failure("SERVER ERROR!")
            }

            override fun networkError(e: IOException) {
                System.err.println("NETWORK ERROR " + e.message)
                viewModeResponse.failure("NETWORK ERROR!")
            }

            override fun unexpectedError(t: Throwable) {
                System.err.println("FATAL ERROR " + t.message)
                viewModeResponse.failure("FATAL ERROR!")
            }
        })
    }


    private fun getRepositories() {
        val call = apiService.repositories(userName,
            Configs.CLIENTID,
            Configs.CLIENTSECRET)
        call.enqueue(object : ErrorHandlingAdapter.MyCallback<ArrayList<RepositoryModel>> {

            override fun clientError(response: Response<*>) {
                viewModeResponse.failure("CLIENT ERROR!")
            }
            override fun success(response: Response<ArrayList<RepositoryModel>>) {
                System.out.println("SUCCESS! " + response.body())
                if (response.body() != null) {
                    repositoriesFlag = true
                    repositoryModel.postValue( response.body()!!)
                    responsePatcher()
                } else {
                    viewModeResponse.failure(response.code().toString())
                }
            }

            override fun unauthenticated(response: Response<*>) {
                println("UNAUTHENTICATED")
                viewModeResponse.failure("UNAUTHENTICATED ERROR!")
            }

            override fun serverError(response: Response<*>) {
                System.out.println("SERVER ERROR " + response.code() + " " + response.message())
                viewModeResponse.failure("SERVER ERROR!")
            }

            override fun networkError(e: IOException) {
                System.err.println("NETWORK ERROR " + e.message)
                viewModeResponse.failure("NETWORK ERROR!")
            }

            override fun unexpectedError(t: Throwable) {
                System.err.println("FATAL ERROR " + t.message)
                viewModeResponse.failure("FATAL ERROR!")
            }
        })
    }

    private fun responsePatcher() {
        if (userFlag && repositoriesFlag) {
           viewModeResponse.success(this)
        }
    }

    fun getRowNumber(): Int {
        return 1 + (repositoryModel.value?.size ?: 0)
    }

}