package com.mobile.theirgithub.viewModel

import com.mobile.theirgithub.utils.DateUtil

data class UserModel (
    var  login:String,
    var  id:Int,
    var  node_id:String,
    var  avatar_url:String,
    var  gravatar_id:String,
    var  url:String,
    var  html_url:String,
    var  followers_url:String,
    var  following_url:String,
    var  gists_url:String,
    var  starred_url:String,
    var  subscriptions_url:String,
    var  organizations_url:String,
    var  repos_url:String,
    var  events_url:String,
    var  received_events_url:String,
    var  type:String,
    var  site_admin:Boolean,
    var  name:String,
    var  company:String,
    var  blog:String?,
    var  location:String,
    var  email:String,
    var  hireable:Boolean,
    var  bio:String,
    var  public_repos:Int,
    var  public_gists:Int,
    var  followers:Int,
    var  following:Int,
    var  created_at:String,
    var  updated_at:String
){


     var tempCreateDate = created_at
         get() {
             if (  !created_at.equals("")){
                return DateUtil.getYear(created_at)
             }else{
                 return "Unknown Date!"
             }
         }

        var alpha = 1f
         get() {
             if (blog != null && !blog.equals("")){
                 return 1f
             }else{
                 return 0.3f
             }
         }

        var blogUnable = true
         get() {
             return (blog != null && !blog.equals(""))
         }
  }




 data class RepositoryModel (
    var id:Int,
    var node_id:String,
    var name:String,
    var language:String?,
    var description:String?,
    var html_url:String,
    var forks_count:Int,
    var stargazers_count:Int,
    var created_at:String,
    var updated_at:String
){

    var language_temp = language
    get() {
        if ( language != null && !language.equals("")){
            return "Language: $language"
        }else{
           return "Language: Not Defined!"
        }
    }

    var description_temp = description
    get() {
        if ( description!= null && !description.equals("")){
            return description
        }else{
            return "No Description!"
        }
    }

    var detail:String = ""
        get() {
           return "$stargazers_count stars and $forks_count forks. For more Information, please visit github "
        }

    //range date betwwen create and update of a repository
    var rangeDate:String = ""
        get() {
            return DateUtil.getYear(created_at) +"-"+ DateUtil.getYear(updated_at)
        }

}


