package com.mobile.theirgithub.interfaces
import com.mobile.theirgithub.viewModel.UserViewModel

interface ViewModeResponse {
    fun success(viewModel: UserViewModel)
    fun failure(msg:String)
}