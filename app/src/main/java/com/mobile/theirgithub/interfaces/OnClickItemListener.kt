package com.mobile.theirgithub.interfaces

interface OnClickItemListener {
    fun onItemClick(url:String?)
}