package com.mobile.theirgithub.adapters


import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mobile.theirgithub.BR
import com.mobile.theirgithub.R
import com.mobile.theirgithub.viewModel.UserViewModel
import com.mobile.theirgithub.databinding.ProjectListBinding
import com.mobile.theirgithub.databinding.UserInfoBinding



class UserInformationAdapter(private val activity: Activity, private val userViewModel: UserViewModel) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>()  {

    private val inflator: LayoutInflater

    init {
        inflator = LayoutInflater.from(activity)
    }

    override fun getItemCount(): Int {
        return userViewModel.getRowNumber()
    }


    override fun getItemViewType(position: Int): Int {
        if(position == 0){
            return 0
        }else{
            return 1
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, type: Int): RecyclerView.ViewHolder {
        when (type) {
             0 -> {
                 val userInfoBinding = DataBindingUtil.inflate<UserInfoBinding>(LayoutInflater.from(viewGroup.getContext()),
                     R.layout.user_info, viewGroup, false);
                 return UserInfoViewholder(userInfoBinding)
             }
             else -> {

                 val projectListBinding = DataBindingUtil.inflate<ProjectListBinding>(LayoutInflater.from(viewGroup.getContext()),
                     R.layout.project_list, viewGroup, false);
                 return RepoViewHolder(projectListBinding)

             }
        }
    }

   override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
       when (viewHolder.getItemViewType()) {
            0 -> {
                val userInfoViewholder: UserInfoViewholder = viewHolder as UserInfoViewholder
                userInfoViewholder.userInfoBinding.setVariable(BR.userViewModel, userViewModel)
            }
           1 -> {
               val repoViewHolder: RepoViewHolder = viewHolder as RepoViewHolder
               repoViewHolder.projectListBinding.setVariable(BR.userViewModel, userViewModel)
               repoViewHolder.projectListBinding.setVariable(BR.indexID, repoViewHolder.adapterPosition-1)
           }
       }
    }



    inner class UserInfoViewholder(userInfoBinding: UserInfoBinding) : RecyclerView.ViewHolder(userInfoBinding.root) {

        var userInfoBinding: UserInfoBinding
        init {
            this.userInfoBinding = userInfoBinding

        }

    }


    inner class RepoViewHolder(projectListBinding: ProjectListBinding) : RecyclerView.ViewHolder(projectListBinding.root){
        var projectListBinding: ProjectListBinding
        init {
            this.projectListBinding = projectListBinding
        }
    }

}



