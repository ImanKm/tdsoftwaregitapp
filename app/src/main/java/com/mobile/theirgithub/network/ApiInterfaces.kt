package com.mobile.theirgithub.network

import retrofit2.http.*
import com.mobile.theirgithub.network.ErrorHandlingAdapter.MyCall
import com.mobile.theirgithub.viewModel.RepositoryModel
import com.mobile.theirgithub.viewModel.UserModel
import retrofit2.http.GET



internal interface ApiInterfaces {

    //get basic information of a user
    @GET("{userName}")
    fun user(@Path("userName") name:String,@Query("client_id") clientId:String,@Query("client_secret") clientSecret:String): MyCall<UserModel>

    //get list of repositories of a user
    @GET("{userName}/repos")
    fun repositories(@Path("userName") name:String,@Query("client_id") clientId:String,@Query("client_secret") clientSecret:String): MyCall<ArrayList<RepositoryModel>>

}

