package com.mobile.theirgithub.network

import android.content.Context
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.mobile.theirgithub.network.ErrorHandlingAdapter.ErrorHandlingCallAdapterFactory




object ApiClient {

    val BASE_URL = "https://api.github.com/users/"
    private var retrofit: Retrofit? = null
    internal var okHttpClient: OkHttpClient? = null
    var interceptor: HttpLoggingInterceptor? = null

    //create okhttp Client. This function act like a singletone pattern
    fun getClient(context: Context): Retrofit {
        if (okHttpClient == null) {
            interceptor = HttpLoggingInterceptor()
            interceptor!!.setLevel(HttpLoggingInterceptor.Level.BODY)
            okHttpClient = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build()
        }
        val gson = GsonBuilder().setLenient().create()
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient!!)
                .build()
        }
        return retrofit!!
    }

}
