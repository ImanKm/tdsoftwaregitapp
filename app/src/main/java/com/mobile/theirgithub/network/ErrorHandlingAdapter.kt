package com.mobile.theirgithub.network

import com.mobile.theirgithub.R
import java.io.IOException
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.concurrent.Executor
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

/**
 * A sample showing a custom [CallAdapter] which adapts the built-in [Call] to a custom
 * version whose callback has more granular methods.
 */
class ErrorHandlingAdapter {
    /** A callback which offers granular callbacks for various conditions.  */
    internal interface MyCallback<T> {
        /** Called for [200, 300) responses.  */
        fun success(response: Response<T>)

        /** Called for 401 responses.  */
        fun unauthenticated(response: Response<*>)

        /** Called for [400, 500) responses, except 401.  */
        fun clientError(response: Response<*>)

        /** Called for [500, 600) response.  */
        fun serverError(response: Response<*>)

        /** Called for network errors while making the call.  */
        fun networkError(e: IOException)

        /** Called for unexpected errors while making the call.  */
        fun unexpectedError(t: Throwable)
    }

    internal interface MyCall<T> {
        fun cancel()
        fun enqueue(callback: MyCallback<T>)
        fun clone(): MyCall<T>
    }

    class ErrorHandlingCallAdapterFactory : CallAdapter.Factory() {
        override fun get(
            returnType: Type, annotations: Array<Annotation>,
            retrofit: Retrofit
        ): CallAdapter<*, *>? {
            if (getRawType(returnType) != MyCall::class.java) {
                return null
            }
            if (returnType !is ParameterizedType) {
                throw IllegalStateException(
                    "MyCall must have generic type (e.g., MyCall<ResponseBody>)"
                )
            }
            val responseType = CallAdapter.Factory.getParameterUpperBound(0, returnType)
            val callbackExecutor = retrofit.callbackExecutor()
            return ErrorHandlingCallAdapter<R>(
                responseType,
                callbackExecutor!!
            )
        }

        private class ErrorHandlingCallAdapter<R> internal constructor(
            private val responseType: Type,
            private val callbackExecutor: Executor
        ) : CallAdapter<R, MyCall<R>> {

            override fun responseType(): Type {
                return responseType
            }

            override fun adapt(call: Call<R>): MyCall<R> {
                return MyCallAdapter(
                    call,
                    callbackExecutor
                )
            }
        }
    }

    /** Adapts a [Call] to [MyCall].  */
    internal class MyCallAdapter<T>(
        private val call: Call<T>,
        private val callbackExecutor: Executor
    ) : MyCall<T> {

        override fun cancel() {
            call.cancel()
        }

        override fun enqueue(callback: MyCallback<T>) {
            call.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    val code = response.code()
                    if (code >= 200 && code < 300) {
                        callback.success(response)
                    } else if (code == 401) {
                        callback.unauthenticated(response)
                    } else if (code >= 400 && code < 500) {
                        callback.clientError(response)
                    } else if (code >= 500 && code < 600) {
                        callback.serverError(response)
                    } else {
                        callback.unexpectedError(RuntimeException("Unexpected response $response"))
                    }
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    if (t is IOException) {
                        callback.networkError(t)
                    } else {
                        callback.unexpectedError(t)
                    }
                }
            })
        }

        override fun clone(): MyCall<T> {
            return MyCallAdapter(
                call.clone(),
                callbackExecutor
            )
        }
    }
}